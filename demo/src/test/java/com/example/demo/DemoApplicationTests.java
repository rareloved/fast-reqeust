package com.example.demo;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SpringBootTest
public class DemoApplicationTests {

    @Test
    public void contextLoads() {
        String url = "http://localhost:8081/demo/user/byId/44";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class);
        // 获取响应的状态
        HttpStatus statusCode = entity.getStatusCode();
        // 获取响应的header信息
        HttpHeaders headers = entity.getHeaders();
        // 获取响应的body信息
        String msg = entity.getBody();
        System.out.println(statusCode);
        System.out.println(headers);
        System.out.println(msg);
    }
    void f(){
        File[] hiddenFiles = new File(".").listFiles(File::isHidden);
        System.out.println();
        List a = new ArrayList<String>();

    }

}
