package com.example.demo.domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * @author andy
 * @date 2020-6-28
 * */
public class DbTest {
    public Message test(String dbname,String ip,String port,String username,String password){
        Boolean flag = false;
        Message message = null;

        DBTypeEnum dbTypeEnum = DBTypeEnum.valueOf(dbname);
        try{
            //加载数据库的驱动类，不同数据库不一样
            Class.forName(dbTypeEnum.driver) ;
        }catch(ClassNotFoundException e){
            System.out.println("找不到驱动程序类 ，加载驱动失败！");
            e.printStackTrace() ;
        }
        //连接MySql数据库，用户名和密码都是root
        //String url = "jdbc:mysql://localhost:3306/test" ;
        StringBuffer urlBuffer = new StringBuffer("");
        //不同数据库的前缀不一样
        urlBuffer.append(dbTypeEnum.prefix);
        urlBuffer.append(ip).append(":").append(port);
        //若是oracle,URL处理+sid
        if(dbTypeEnum.dbname.equals(DBTypeEnum.Oracle11C.dbname)){
            urlBuffer.append(":orcl");
        }
        String url = urlBuffer.toString();
        try{
            Connection con = DriverManager.getConnection(url,username,password);
            con.close();
            flag = true;
            message = new Message("数据库连接成功！",Boolean.TRUE);
            System.out.println(dbname+"数据库连接成功！");
        }catch(SQLException se){
            message = new Message(se.getMessage(),Boolean.FALSE);
            System.out.println(dbname+"数据库连接失败！");
            se.printStackTrace();
        }
        return message;
    }

    public Message test(String dbname,String ip,String port,String username,String password,String catalog){
        Boolean flag = false;
        Message message = null;

        DBTypeEnum dbTypeEnum = DBTypeEnum.valueOf(dbname);
        try{
            //加载数据库的驱动类，不同数据库不一样
            Class.forName(dbTypeEnum.driver) ;
        }catch(ClassNotFoundException e){
            System.out.println("找不到驱动程序类 ，加载驱动失败！");
            e.printStackTrace() ;
        }
        //连接MySql数据库，用户名和密码都是root
        //String url = "jdbc:mysql://localhost:3306/test" ;
        StringBuffer urlBuffer = new StringBuffer("");
        //不同数据库的前缀不一样
        urlBuffer.append(dbTypeEnum.prefix);
        urlBuffer.append(ip).append(":").append(port);
        //若是oracle,URL处理+sid
//        if(dbTypeEnum.dbname.equals(DBTypeEnum.Oracle11C.dbname)){
//            urlBuffer.append(":orcl");
//        }
        switch (dbTypeEnum){
            case Oracle11C:urlBuffer.append(catalog==null?"":"/"+catalog);break;
//            case Oracle11C:urlBuffer.append(":orcl");break;
            case DM:break;
            case Mysql:urlBuffer.append(catalog==null?"":"/"+catalog).append("?serverTimezone=Asia/Shanghai");break;
            default:break;
        }

        String url = urlBuffer.toString();
        try{
            Connection con = DriverManager.getConnection(url,username,password);
            con.close();
            flag = true;
            message = new Message("数据库连接成功！",Boolean.TRUE);
            System.out.println(dbname+"数据库连接成功！");
        }catch(SQLException se){
            message = new Message(se.getMessage(),Boolean.FALSE);
            System.out.println(dbname+"数据库连接失败！");
            se.printStackTrace();
        }
        return message;
    }

    public static void main(String[] args) {
        DbTest test = new DbTest();
        Message message = test.test("DM","127.0.0.1","5236","CRM","888888888");//DM
//        Message message = test.test("Oracle","192.168.4.97","1521","wzdd2015","wzdd2015");//mysql
//        Message message = test.test("Oracle11C","192.168.2.1","1521","andy","abc123","pdb1");//mysql
//        Boolean flag = test.test("MYSQL","192.168.2.1","3306","root","root");//dm
        if(message.getStatus()){
            System.out.println("数据库连接成功！");
        }else{
            System.out.println("连接失败！"+message.getMsg());
        }
    }
}
