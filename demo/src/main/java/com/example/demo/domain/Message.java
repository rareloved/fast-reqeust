package com.example.demo.domain;

/*
* 数据库连接检测异常信息封装
* @author andy
* @date 2020-7-1
* */
public class Message {
    private String msg;
    private Boolean status;
    public Message(String msg, Boolean status){
        this.msg = msg;
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public Boolean getStatus() {
        return status;
    }
}
