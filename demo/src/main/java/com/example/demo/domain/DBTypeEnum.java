package com.example.demo.domain;


/*
* @author andy
* @date 2020-6-28
*
* url ip:port/dbname
* */
public enum DBTypeEnum {

    DB2("DB2","jdbc:db2://","com.ibm.db2.jcc.DB2Driver"),
    SQLServer("SQLServer","jdbc:sqlserver://","com.mircosoft.sqlserver.jdbc.SQLServerDriver"),
    Oracle11C("Oracle11C","jdbc:oracle:thin:@","oracle.jdbc.OracleDriver"),
    Oracle12C("Oracle12C","jdbc:oracle:thin:@","oracle.jdbc.OracleDriver"),
    Mysql("Mysql","jdbc:mysql://","com.mysql.jdbc.Driver"),
    DM("DM","jdbc:dm://","dm.jdbc.driver.DmDriver");
    public String dbname;
    public String prefix;
    public String driver;
    DBTypeEnum(String dbname,String prefix,String driver){
        this.dbname = dbname;
        this.prefix = prefix;
        this.driver = driver;
    }



    public static void main(String[] args) {
        System.out.println(DBTypeEnum.valueOf("DB2").prefix);
    }
}
