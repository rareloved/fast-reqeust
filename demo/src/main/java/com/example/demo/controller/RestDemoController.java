package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

/**
 * @description
 * @author: andy
 * @create: 2021-01-23 18:45
 **/
@RestController
@RequestMapping(value = "/user")
public class RestDemoController {
    @GetMapping("/byId/{id}")
    public String getUser(@PathVariable String id){
        System.out.println("id:"+id);
        return id;
    }
}
